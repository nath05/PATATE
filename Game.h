// "Copyright 2018 Nathan Poirier"
#include "Player.h"
#include "Laser.h"
#include "World.h"
#include "SFML/Graphics.hpp"

#ifndef GAME_H_
#define GAME_H_

#define SFML_STATIC
#define LASER 50

class Game {
 public:
  Game();
  void run();
  void processEvents();
  void update();
  void render();
  void camera();
  void laserHandle();

 private:
  sf::RenderWindow window;
  clock_t laserT_B;
  clock_t laserT_E;
  sf::Clock clock;
  sf::Time time;
  Player player;
  Laser laser[LASER];
  World world;
  int i;
};

#endif  // GAME_H_
