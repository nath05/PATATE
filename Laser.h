// "Copyright 2018 Nathan Poirier"
#include "Player.h"
#include <SFML/Graphics.hpp>

#ifndef LASER_H_
#define LASER_H_

#define LASERH 3
#define LASERW 10

class Laser {
 public:
  Laser();
  ~Laser();

  void update(float dt);
  void draw(sf::RenderWindow &window);
  float getposx();
  float getposy();
  float getvelx();
  void setpos(float m_posx, float m_posy);
  void setvelx(float m_velx);

 private:
  sf::Texture laserTexture;
  sf::Sprite laserSprite;
  float posx;
  float posy;
  float velx;
};


#endif  // LASER_H_
