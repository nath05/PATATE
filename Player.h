// "Copyright 2018 Nathan Poirier"
#include <string>
#include <SFML/Graphics.hpp>

#ifndef PLAYER_H_
#define PLAYER_H_

// le sprite a la base est 50x50, donc changer en fonction du scaling
#define PLAYERH 100
#define PLAYERW 100

#define WIDTH 1600
#define HEIGHT 700

class Player {
 public:
  Player();
  ~Player();

  void movement();
  void update(float dt);
  void draw(sf::RenderWindow &window);
  float getposx();
  float getposy();
  float getvelx();
  float getvely();
  std::string getFacing();
  int getLaserState();
  void setposx(float m_posx);
  void setposy(float m_posy);
  void setvelx(float m_velx);
  void setvely(float m_vely);
  void boundaries();
  void keyHandle();
  void setState(std::string m_state);
  void stateHandle();
  void textureHandle();
  void facingHandle();

 private:
  sf::Texture playerTexture;
  sf::Sprite playerSprite;
  float maxVel;
  float posx;
  float posy;
  float velx;
  float vely;
  float gravity;
  float friction;
  std::string state;
  int laserState;
  std::string facing;
  float jumpHeight;
  float groundPos;
};


#endif  // PLAYER_H_
