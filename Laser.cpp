// "Copyright 2018 Nathan Poirier"
#include "Laser.h"
#include <iostream>

using namespace std;

Laser::Laser() :
posx(0), posy(-10), velx(0) {
laserTexture.loadFromFile("assets/laser.png");
laserSprite.setTexture(laserTexture);
}

Laser::~Laser() {
}

float Laser::getposx() {
  return posx;
}
float Laser::getposy() {
  return posy;
}
float Laser::getvelx() {
  return velx;
}
void Laser::setpos(float m_posx, float m_posy) {
  posx = m_posx;
  posy = m_posy;
}
void Laser::setvelx(float m_velx) {
  velx = m_velx;
}

void Laser::update(float dt) {
  posx += velx;
  laserSprite.setPosition(posx, posy);
}

void Laser::draw(sf::RenderWindow &window) {
  window.draw(laserSprite);
}
